
program rk4
	use m_grid, only: get_h, grid, diff_mat1, diff_mat2, laplacian
	use m_kinds, only: qp, hk
	use m_errors, only: compute_error
	implicit none

	real(kind=hk) :: ax, bx, ay, by
	real(kind=hk) :: ti, tf, ttotal
	real(kind=hk) :: alpha
	real(kind=hk) :: dt, hx, hy
	real(kind=hk) :: amplitude, std_dev, center
	real(kind=hk), allocatable :: xs(:), ys(:), Dxx(:, :), Dyy(:, :), L(:, :)
	real(kind=hk), allocatable :: un(:), unp1(:)
	real(kind=qp), allocatable :: uexact(:)
	real(kind=hk), allocatable :: k1(:), k2(:), k3(:), k4(:), tmp(:)

	integer :: nt, nx
	integer :: i, j 

	character(len=32) :: arg
	character(len=50) :: fname
	real :: start, finish

	!Read in nt and nx
	call getarg(1, arg)
	read(arg, *)nt 
	call getarg(2, arg)
	read(arg, *)nx

	!Initial conditions 
	ax = -1.0_hk
	bx = 1.0_hk
	ay = -1.0_hk
	by = 1.0_hk
	ti = 0.0_hk
	tf = 100.0_hk
	dt = (tf - ti) / real(nt, hk)
	alpha = 1e-2_hk
	amplitude = 5.0_hk
	std_dev = 0.5_hk
	center = 0.9_hk

	hx = get_h(ax, bx, nx) !square grid because lazy
	hy = get_h(ay, by, nx)
	xs = grid(ax, bx, nx, hx)
	ys = grid(ay, by, nx, hy)
	Dxx = diff_mat2(nx, hx)
	Dyy = diff_mat2(nx, hy)
	L = laplacian(nx, Dxx, Dyy)

	allocate(un(nx*nx))
	do j = 1,nx
		do i = 1,nx 
	  	un(i + nx*j) = amplitude * &
	    	exp(-((xs(i) - 0.5_hk*center)**2 + (ys(j) - 0.5_hk*center)**2) / std_dev)
		end do
	end do 
	allocate(unp1(nx*nx))
	allocate(uexact(nx*nx))
	allocate(k1(nx*nx))
	allocate(k2(nx*nx))
	allocate(k3(nx*nx))
	allocate(k4(nx*nx))
	allocate(tmp(nx*nx))

	!Begin time stepping
	call cpu_time(start)
	ttotal = ti 
	do i = 1,nt 
		if (i == nt) then 
			dt = tf - ttotal
		end if

		k1 = fi(L, un, alpha)
		tmp = un + 0.5*dt * k1
		k2 = fi(L, tmp, alpha)
		tmp = un + 0.5*dt * k2
		k3 = fi(L, tmp, alpha)
		tmp = un + dt * k3
		k4 = fi(L, tmp, alpha)

		unp1 = un + dt/6.0_hk * (k1 + 2.0_hk * k2 + 2.0_hk * k3 + k4)
		un(:) = unp1(:)
		ttotal = ttotal + dt 
	end do

	call cpu_time(finish)
	print *, "method duration: ", finish-start

	write (fname, "(A7,I2,A4)") "ref_sol", nx, ".txt"
	open(unit=12, file=fname)
	read (12,*) uexact(:)
	close(12)
	print *, "method error: ", compute_error(uexact, real(un, qp), real(hx, qp))


contains

	function fi(D, u, alpha) result(f)
		real(kind=hk), intent(in) :: D(:,:), u(:), alpha

		real(kind=hk) :: f(size(u))

		f = alpha * matmul(D, u) ! f = alpha D * u AKA [alpha (d^2 / dx^2 + d^2 / dy^2) u
	end function fi

end program rk4
