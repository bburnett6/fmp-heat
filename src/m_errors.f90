module m_errors
  use m_kinds, only: qp
  implicit none

  private

  public :: compute_error
contains
  
  function compute_error(ue, un, h) result(e)
    real(kind=qp), intent(in) :: ue(:), un(:), h

    real(kind=qp) :: e, tmp(size(ue))
    real(kind=qp) :: dot_sum = 0.0_qp

    integer :: i, n 
    n = size(ue)

    !Compute dot product of ue - un
    tmp = ue - un 
    do i=1,n 
      dot_sum = dot_sum + tmp(i)*tmp(i)
    end do

    e = sqrt(h * dot_sum)

  end function compute_error

end module m_errors
