#ifndef HKIND
#define HKIND real64
#endif
#ifndef LKIND
#define LKIND real64
#endif

module m_kinds
	use iso_fortran_env, only: real128, real64, real32
	implicit none
	public
	integer, parameter :: hk = HKIND ! High precision kind
	integer, parameter :: lk = LKIND ! Low precision kind

	integer, parameter :: qp = real128 ! Need to export quadruple precision for calculating errors
	integer, parameter :: dp = real64 ! Include others for tests
	integer, parameter :: sp = real32

end module m_kinds