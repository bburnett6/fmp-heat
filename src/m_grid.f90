
module m_grid
  !use stdlib_linalg, only: diag, kronecker_product
  use m_linalg, only: diagH, kronH
  use m_kinds, only: hk
  implicit none
  private
  
  public :: get_h, grid, diff_mat1, diff_mat2, laplacian

contains

  function get_h(a, b, n) result(h)
    implicit none
    real(kind=hk), intent(in) :: a, b 
    integer, intent(in) :: n 
    real(kind=hk) :: h 

    h = (b - a) / real(n, hk)

  end function get_h

  function grid(a, b, n, h) result(xs)
    implicit none
    real(kind=hk), intent(in) :: a, b, h 
    integer, intent(in) :: n 
    real(kind=hk), allocatable :: xs(:)

    integer :: i 

    allocate(xs(n))
    xs = [(a + h*i, i = 0, n-1)]

  end function grid

  function diff_mat1(n, h) result(Dx)
    implicit none
    integer, intent(in) :: n 
    real(kind=hk), intent(in) :: h 
    real(kind=hk), allocatable :: Dx(:, :)

    integer :: i 

    allocate(Dx(n, n))
    !Periodic First order differentiation matrix
    ! [  0  1  0  0 -1 ]
    ! [ -1  0  1  0  0 ]
    ! [  0 -1  0  1  0 ]
    ! [  0  0 -1  0  1 ]
    ! [  1  0  0 -1  0 ]
    !Dx = diag([(-1.0_hk, i = 1, n - 1)], -1) + diag([(1.0_hk, i = 1, n - 1)], 1)
    Dx = diagH([(-1.0_hk, i = 1, n - 1)], -1) + diagH([(1.0_hk, i = 1, n - 1)], 1)
    Dx(1, n) = -1.0_hk
    Dx(n, 1) = 1.0_hk
    Dx(:, :) = Dx(:, :) / (2.0_hk * h)

  end function diff_mat1

  function diff_mat2(n, h) result(Dxx)
    implicit none
    integer, intent(in) :: n 
    real(kind=hk), intent(in) :: h 
    real(kind=hk), allocatable :: Dxx(:, :)

    integer :: i 

    allocate(Dxx(n, n))
    !Periodic Second order differentiation matrix
    ! [ -2  1  0  0  1 ]
    ! [  1 -2  1  0  0 ]
    ! [  0  1 -2  1  0 ]
    ! [  0  0  1 -2  1 ]
    ! [  1  0  0  1 -2 ]
    !Dxx = diag([(1.0_hk, i = 1, n - 1)], -1) + &
    !  diag([(-2.0_hk, i = 1, n)], 0) + &
    !  diag([(1.0_hk, i = 1, n - 1)], 1)
    Dxx = diagH([(1.0_hk, i = 1, n - 1)], -1) + &
      diagH([(-2.0_hk, i = 1, n)], 0) + &
      diagH([(1.0_hk, i = 1, n - 1)], 1)
    Dxx(1, n) = 1.0_hk
    Dxx(n, 1) = 1.0_hk
    Dxx(:, :) = Dxx(:, :) / (h*h)

  end function diff_mat2

  function laplacian(n, Dxx, Dyy) result(L)
    !Note: This only works with square grids. Need to refactor to work with rectangular grids.
    implicit none
    integer, intent(in) :: n 
    real(kind=hk), intent(in) :: Dxx(n, n), Dyy(n, n)
    real(kind=hk), allocatable :: L(:, :)

    integer :: i 
    real(kind=hk) :: Eye(n, n)

    !Eye = diag([(1.0_hk, i = 1, n)])
    Eye = diagH([(1.0_hk, i = 1, n)], 0)
    allocate(L(n*n, n*n))

    !L = kronecker_product(Eye, Dxx) + kronecker_product(Dyy, Eye)
    L = kronH(Eye, Dxx) + kronH(Dyy, Eye)

  end function laplacian

end module m_grid