module m_linalg
	use m_kinds, only: hk, lk
	implicit none
	private

	public :: diagH, diagL, kronH, kronL
contains

! Reimplementations of the fortran stdlib_linalg functions diag and kronecker_product
! At the time of this writing, the fortran stdlib only has support for single precision (sp)
! and double precision (dp). These were implemented for support for quadruple precision (qp)

	function diagH(v,k) result(res)
		real(kind=hk), intent(in) :: v(:)
        integer, intent(in) :: k
        real(kind=hk) :: res(size(v)+abs(k),size(v)+abs(k))
        integer :: i, sz
        sz = size(v)
        res = 0
        if (k > 0) then
          do i = 1, sz
              res(i,k+i) = v(i)
          end do
        else if (k < 0) then
          do i = 1, sz
              res(i+abs(k),i) = v(i)
          end do
        else
          do i = 1, sz
              res(i,i) = v(i)
          end do
        end if
	end function diagH

	function diagL(v,k) result(res)
		real(kind=lk), intent(in) :: v(:)
        integer, intent(in) :: k
        real(kind=lk) :: res(size(v)+abs(k),size(v)+abs(k))
        integer :: i, sz
        sz = size(v)
        res = 0
        if (k > 0) then
          do i = 1, sz
              res(i,k+i) = v(i)
          end do
        else if (k < 0) then
          do i = 1, sz
              res(i+abs(k),i) = v(i)
          end do
        else
          do i = 1, sz
              res(i,i) = v(i)
          end do
        end if
	end function diagL

	function kronH(A, B) result(C)
		real(kind=hk), intent(in) :: A(:,:), B(:,:)
		real(kind=hk) :: C(size(A,dim=1)*size(B,dim=1), size(A,dim=2)*size(B,dim=2))

		integer :: m1, n1, maxM1, maxN1, maxM2, maxN2

		maxM1 = size(A, dim=1)
		maxN1 = size(A, dim=2)
		maxM2 = size(B, dim=1)
		maxN2 = size(B, dim=2)

		do n1 = 1, maxN1
			do m1 = 1, maxM1
				C((m1-1)*maxM2+1:m1*maxM2, (n1-1)*maxN2+1:n1*maxN2) = A(m1, n1) * B(:,:)
			end do 
		end do
	end function kronH

	function kronL(A, B) result(C)
		real(kind=lk), intent(in) :: A(:,:), B(:,:)
		real(kind=lk) :: C(size(A,dim=1)*size(B,dim=1), size(A,dim=2)*size(B,dim=2))

		integer :: m1, n1, maxM1, maxN1, maxM2, maxN2

		maxM1 = size(A, dim=1)
		maxN1 = size(A, dim=2)
		maxM2 = size(B, dim=1)
		maxN2 = size(B, dim=2)

		do n1 = 1, maxN1
			do m1 = 1, maxM1
				C((m1-1)*maxM2+1:m1*maxM2, (n1-1)*maxN2+1:n1*maxN2) = A(m1, n1) * B(:,:)
			end do 
		end do
	end function kronL

end module m_linalg