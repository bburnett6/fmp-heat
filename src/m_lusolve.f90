
module m_lusolve
	use m_kinds, only: lk ! the linear solves will be done primarily in low precision. If high and low are needed, will need to refactor
	implicit none
	private

	public :: ludcmp, lubksb

contains
	
	subroutine ludcmp(A, n, np, indx, d)
		!Implemented from Numerical Recipes in Fortran77. See ch 2.3, pg 38 for full details
		implicit none
		integer, intent(in) :: n, np
		integer, intent(inout) :: indx(n)
		real(kind=lk), intent(inout) :: A(np, np), d 

		integer, parameter :: nmax=40000
		integer :: i, imax, j, k 
		real(kind=lk) :: tiny, aamax, dum, sum, vv(NMAX)

		tiny = epsilon(0.0_lk) !machine epsilon for the precision we operate with
		d = 1.0

		!print *, "Part 1"
		do i = 1,n 
			aamax = 0.0_lk
			do j = 1,n 
				if (abs(a(i,j)) .gt. aamax) then
					aamax = abs(a(i,j))
				end if
			end do 
			if (aamax .eq. 0) then 
				print *, 'singular matrix in ludcmp'
			endif
			vv(i) = 1.0_lk / aamax
		end do 

		!print *, "Part 2"

		do j = 1,n 
			!print *, "Part 2a"
			do i = 1,j-1 
				sum = a(i, j)
				do k = 1,i-1
					sum = sum - a(i,k) * a(k,j)
				end do 
				a(i, j) = sum
			end do 
			aamax = 0.0_lk
			!print *, "Part 2b"
			do i = j,n 
				sum = a(i,j)
				do k = 1,j-1
					sum = sum - a(i,k) * a(k,j)
				end do
				a(i,j) = sum
				dum = vv(i) * abs(sum)
				if (dum .ge. aamax) then
					imax = i 
					aamax = dum
				end if
			end do 
			!print *, "Part 2c"
			if (j .ne. imax) then 
				do k = 1,n 
					dum = a(imax, k)
					a(imax,k) = a(j,k)
					a(j,k) = dum
				end do 
				d = -d 
				vv(imax) = vv(j)
			end if 
			indx(j) = imax 
			!print *, "Part 2d"
			if (a(j,j) .eq. 0.0_lk) then
				a(j,j) = tiny
			end if
			if (j .ne. n) then
				dum = 1.0_lk / a(j,j)
				do i = j+1,n 
					a(i,j) = a(i,j) * dum
				end do
			end if
		end do 
		
		return
	end subroutine ludcmp

	subroutine lubksb(A, n, np, indx, b)
		!Implemented from Numerical Recipes in Fortran77. See ch 2.3 pg 39 for full details
		implicit none
		integer, intent(in) :: n, np 
		integer, intent(inout) :: indx(n)
		real(kind=lk), intent(inout) :: b(n), A(np, np)

		integer :: i, ii, j, ll 
		real(kind=lk) :: sum 

		ii = 0
		do i = 1,n 
			ll = indx(i)
			sum = b(ll)
			b(ll) = b(i)
			if (ii .ne. 0) then 
				do j = ii, i-1
					sum = sum - a(i,j) * b(j)
				end do
			else if (sum .ne. 0.0_lk) then 
				ii = i 
			end if
			b(i) = sum
		end do 

		do i = n,1,-1 
			sum = b(i)
			do j = i+1,n 
				sum = sum - a(i,j) * b(j)
			end do 
			b(i) = sum / a(i,i)
		end do 

		return

	end subroutine lubksb

end module m_lusolve