module fmp_heat
  implicit none
  private

  public :: say_hello
contains
  subroutine say_hello
    print *, "Hello, fmp-heat!"
  end subroutine say_hello

end module fmp_heat
