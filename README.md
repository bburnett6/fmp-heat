# Fortran Mixed-Precision Heat Equation

In this code base, we aim to solve the 2 Dimensional Heat Equation

$$
U_t = \alpha \nabla ^2 U
$$

using Mixed-Precision Implicit Runge-Kutta methods.

## Installation

Development and testing of this project was done using the [Fortran Package Manager](https://github.com/fortran-lang/fpm). Please see the official documentation for all the methods of installing `fpm`, but here we outline the installation method used for our development.

```
python -m venv fortran-env
. fortran-env/bin/activate
pip install fpm
```

Because this project is self contained, all that is left to do is to clone this repository and run the codes.

## Running

These codes use a style of mixed-precision programming that relies on the C-Preprocessor flag of `gfortran`. The ideal way to run the applications is as follows

```
fpm run <method> --flag "-cpp -DHKIND=<high precision real kind> -DLKIND=<low precision real kind>"
```

Because we make use of `iso_fortran_env` for handling the real kinds, `HKIND` and `LKIND` should be any combination of `real128`, `real64`, or `real32` (on arm there is theoretically `real16` but that has not been tested yet). An example would be

```
fpm run imr --flag "-cpp -DHKIND=real128 -DLKIND=real64"
```
