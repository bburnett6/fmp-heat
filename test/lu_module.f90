program check
use m_lusolve, only: ludcmp, lubksb
use m_kinds, only: lk 
implicit none

real(kind=lk) :: A(3, 3), b(3), xexact(3)
real(kind=lk) :: Alarge(50, 50), Alarge_old(50, 50)
real(kind=lk) :: blarge(50), blarge_old(50), tmp(50)
integer :: indx(3), indx2(50), i, count
real(kind=lk) :: d 

!A = | 2  1  3 |
!    | 4  5  6 |
!    | 7  8  9 |

A(1, 1) = 2.0_lk
A(1, 2) = 1.0_lk
A(1, 3) = 3.0_lk
A(2, 1) = 4.0_lk
A(2, 2) = 5.0_lk
A(2, 3) = 6.0_lk
A(3, 1) = 7.0_lk
A(3, 2) = 8.0_lk
A(3, 3) = 9.0_lk

!b = | 4 |
!    | 7 |
!    | 2 |

b(1) = 4.0_lk
b(2) = 7.0_lk
b(3) = 2.0_lk

!Check solution against numpy
!>>> import numpy as np
!>>> a = np.array([[2., 1., 3.], [4., 5., 6.], [7., 8., 9.]])
!>>> b = np.array([4., 7., 2.])
!>>> np.linalg.solve(a, b)
!array([-8.33333333, -0.33333333,  7.        ])

xexact(1) = -8.333333_lk
xexact(2) = -0.333333_lk
xexact(3) = 7.0_lk

print *, "Starting Decomp"
call ludcmp(A, 3, 3, indx, d) !A is mutated into its LU form
print *, "A Post Decomp"
print *, A(1, :)
print *, A(2, :)
print *, A(3, :)
print *, "Starting back subst"
call lubksb(A, 3, 3, indx, b) !Solution is overwritten in b

print *, "Expected solution: ", xexact
print *, "Solver solution: ", b 

call random_number(Alarge)
Alarge_old(:, :) = Alarge(:, :)
call random_number(blarge)
blarge_old(:) = blarge(:)

call ludcmp(Alarge, 50, 50, indx2, d)
call lubksb(Alarge, 50, 50, indx2, blarge)

tmp = matmul(Alarge_old, blarge)
count = 0
do i = 1,50
	if (tmp(i) - blarge_old(i) .ge. 1e-6_lk) then
		print *, tmp(i), blarge_old(i)
		count = count + 1
	end if
end do
print *, "Wrong elements: ", count

end program check
