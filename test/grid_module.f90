program grid_module
use m_kinds, only: hk
use m_grid, only: get_h, grid, diff_mat1, diff_mat2, laplacian
implicit none

real(kind=hk) :: h, h_expected
real(kind=hk) :: x(5), x_expected(5)
real(kind=hk) :: Dx(5, 5), Dx_expected(5, 5)
real(kind=hk) :: Dxx(5, 5), Dxx_expected(5, 5)
real(kind=hk) :: L(25, 25)
real(kind=hk) :: a, b 

! Test h
a = 0.0
b = 1.0
h_expected = 0.2_hk !Interesting to see effects of rounding if you forget to add the _hk here
h = get_h(a, b, 5)
print *, "Expected h: ", h_expected
print *, "Function h: ", h 

! Test x
x_expected = (/ 0.0_hk, 0.2_hk, 0.4_hk, 0.6_hk, 0.8_hk /)
x = grid(a, b, 5, h)
print *, "Expected x: ", x_expected
print *, "Function x: ", x 

! Test Dx
Dx_expected(1, :) = (/ 0., 1., 0., 0., -1. /)
Dx_expected(2, :) = (/ -1., 0., 1., 0., 0. /)
Dx_expected(3, :) = (/ 0., -1., 0., 1., 0. /)
Dx_expected(4, :) = (/ 0., 0., -1., 0., 1. /)
Dx_expected(5, :) = (/ 1., 0., 0., -1., 0. /)
Dx_expected(:, :) = Dx_expected(:, :) / (2.0 * h_expected)
Dx = diff_mat1(5, h) 
print *, "Expected Dx:"
print *, Dx_expected(1, :)
print *, Dx_expected(2, :)
print *, Dx_expected(3, :)
print *, Dx_expected(4, :)
print *, Dx_expected(5, :)
print *, "Function Dx:"
print *, Dx(1, :)
print *, Dx(2, :)
print *, Dx(3, :)
print *, Dx(4, :)
print *, Dx(5, :)

! Test Dxx
Dxx_expected(1, :) = (/ -2., 1., 0., 0., 1. /)
Dxx_expected(2, :) = (/ 1., -2., 1., 0., 0. /)
Dxx_expected(3, :) = (/ 0., 1., -2., 1., 0. /)
Dxx_expected(4, :) = (/ 0., 0., 1., -2., 1. /)
Dxx_expected(5, :) = (/ 1., 0., 0., 1., -2. /)
Dxx_expected(:, :) = Dxx_expected(:, :) / (h_expected * h_expected)
Dxx = diff_mat2(5, h) 
print *, "Expected Dxx:"
print *, Dxx_expected(1, :)
print *, Dxx_expected(2, :)
print *, Dxx_expected(3, :)
print *, Dxx_expected(4, :)
print *, Dxx_expected(5, :)
print *, "Function Dxx:"
print *, Dxx(1, :)
print *, Dxx(2, :)
print *, Dxx(3, :)
print *, Dxx(4, :)
print *, Dxx(5, :)

end program grid_module
