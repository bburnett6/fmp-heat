program check
use m_kinds, only: hk, lk, qp, dp, sp
implicit none

print *, "Quad precision: ", qp
print *, "Double precision: ", dp
print *, "Single precision: ", sp

print *, "High precision: ", hk
print *, "Low precision: ", lk
end program check
